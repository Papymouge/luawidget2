luaWidget = {}
V_LUAWIDGET = 2.010
_ALEFT = 1
_ARIGHT = 2
_ACENTER = 3
_ATRONQ = 4
_AHORIZONTAL = 5
_AVERTICAL = 6
_MSLIDER = 1
_MPROGRESS = 2
_MLIFTER = 3
_CWHITE = Color.new(31,31,31)
_CGRAY = Color.new(20,20,20)
_CDARKGRAY = Color.new(5,5,5)
_CBLUE = Color.new(3,0,30)
_CPURPLE = Color.new(20,5,27)
_CBLACK = 0
_NONE = -1000
_CENTER = -1001
_BSTART = 1
_BSELECT = 2
_BA = 3
_BB = 4
_BX = 5
_BY = 6
_BL = 7
_BR = 8
_BUP = 9
_BDOWN = 10
_BLEFT = 11
_BRIGHT = 12

luaWidget.ver = V_LUAWIDGET

--[[ Class Fenetre ]]--
luaWidget.window = {screen = SCREEN_DOWN, visible = true, x=0, y=0, width=256, height=192, cbackground=_NONE}
function luaWidget.window:new(o)
	o = o or {}
	setmetatable(o,self)
	self.__index = self
	o.canvas = Canvas.new()
	o.lobj = {}
	o.background = 0
	if(type(o.cbackground) == "table" and #o.cbackground == 4) then
		o.background = Canvas.newGradientRect(o.x, o.y, o.x+o.width, o.y+o.height,
			o.cbackground[1], o.cbackground[2], o.cbackground[3], o.cbackground[4])
	else
		o.background = Canvas.newGradientRect(o.x, o.y, o.x+o.width, o.y+o.height,
			o.cbackground, o.cbackground, o.cbackground, o.cbackground)
		local buff = o.cbackground
		o.cbackground = {}
		for i=1,4 do o.cbackground[i] = buff end
	end
	Canvas.add(o.canvas, o.background)
	if(o.cbackground[1] == _NONE) then Canvas.setAttr(o.background, ATTR_VISIBLE, false) end
	return o
end
function luaWidget.window:modifObject(o)
	local i,j,k
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			if(i == "cbackground") then
				self.cbackground = {}
				if(type(j) == "table")then
					for k=1,4 do self.cbackground[k] = j[k] end
				else
					for k=1,4 do self.cbackground[k] = j end
				end
				if(j == _NONE) then self.visible = false
				else self.visible = true end
			else
				self[i] = j
			end
		end
	end
	Canvas.setAttr(self.background, ATTR_X1, self.x)
    Canvas.setAttr(self.background, ATTR_Y1, self.y)
    Canvas.setAttr(self.background, ATTR_X2, self.x+self.width)
    Canvas.setAttr(self.background, ATTR_Y2, self.y+self.height)
    Canvas.setAttr(self.background, ATTR_COLOR1, self.cbackground[1])
    Canvas.setAttr(self.background, ATTR_COLOR2, self.cbackground[2])
    Canvas.setAttr(self.background, ATTR_COLOR3, self.cbackground[3])
    Canvas.setAttr(self.background, ATTR_COLOR4, self.cbackground[4])
    Canvas.setAttr(self.background, ATTR_VISIBLE, self.visible)
end
function luaWidget.window:addObjects(o)
	local i, j 
	for j,i in pairs(o) do
		assert(i.parent == 0,"Object["..j.."] is allready associate to a window!")
		table.insert(self.lobj,i)
		i.parent = self
		i:prep(true)
	end
end
function luaWidget.window:removeObjects(o)
	local i, j
	for _,i in pairs(o) do
		for j=1, #self.lobj do
			if(self.lobj[j] == i) then 
				table.remove(self.lobj,j)
				break
			end
		end
		i:remove()
	end
end
function luaWidget.window:held()
	local i , k_re, isOn
	local x, y =  Stylus.X, Stylus.Y
	for _,i in pairs(self.lobj) do
		k_re = square(i.x, i.y, i.x+i.width, i.y+i.height)
		isOn = estDedans(x,y,k_re)
		if(i.visible) then
			if(Stylus.newPress) then
				if(i.active) then
					if(isOn) then
						if(i.click ~= nil) then i:click() end
					end
				end
			elseif(Stylus.held) then
				if(i.held ~= nil) then i:held(isOn) end
			elseif(Stylus.released) then
				if(i.released ~= nil) then i:released(isOn) end
			end
			if(Stylus.doubleClick) then
				if(i.active) then
					if(isOn) then
						if(i.dblClick ~= nil) then i:dblClick() end
					end
				end
			end
			if(i.heldKeys ~= nil) then i:heldKeys() end
		end
	end
end
function luaWidget.window:show()
	local i
	for _,i in pairs(self.lobj) do
		if(i.modif) then
			i:prep()
		end
	end
	Canvas.draw(self.screen, self.canvas, 0,0)
end
function luaWidget.window:destroy()
	local i 
	for _,i in pairs(self.lobj) do
		i:destroy()
	end
	Canvas.destroy(self.canvas)
	collectgarbage("collect")
end

--[[ Class objets ]]--
luaWidget.object = {x = 0, y = 0, width = 100, height = 10, modif = true, visible=true, active=true, parent = 0}
function luaWidget.object:new(o)
	o = o or {}
	setmetatable(o,self)
	self.__index = self
	return o
end
function luaWidget.object:changeBox(o)
	local i,j
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
		end
	end
end
function luaWidget.object:destroy()
	if(self.image ~= nil) then
		if(type(self.image)) == "image" then
			Image.destroy(self.image)
		end
	end
	if(self.limage ~= nil) then
		local i
		for i=1, #self.limage do
			Image.destroy(self.limage[i])
		end
	end
	self:remove()
	self = nil
end
function luaWidget.object:setObjOnTop()
	local i
	for i=1, #self.canvObj do
		Canvas.setObjOnTop(self.parent,self.canvObj[i])
	end
end
function luaWidget.object:onClick()
end
function luaWidget.object:onDblClick()
end
function luaWidget.object:onRelease()
end
function luaWidget.object:onValChange()
end

--[[ Class Object Image ]]--
luaWidget.Timage = luaWidget.object:new{image = ""}
function luaWidget.Timage:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	o.canvObj = {}
	o.canvObj[1] = Canvas.newImage(0,0,0,0,0,0,0)
	if(o.image ~= "") then
        if(o.image ~= _NONE) then
            o.image = Image.load(o.image,VRAM)
        end
    end
	return o
end
function luaWidget.Timage:modifObject(o)
	local i,j
	local chg = false
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
		end
		if(i == "image") then
            if(self.image ~= "") then
                Image.destroy(self.image)
            end
            if(j == _NONE) then self.image = ""
            else
                self.image = Image.load(j,VRAM)
            end
        end
	end
	self.modif = true
end
function luaWidget.Timage:prep(create)
	Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x)
    Canvas.setAttr(self.canvObj[1], ATTR_Y1, self.y)
    Canvas.setAttr(self.canvObj[1], ATTR_IMAGE, self.image)
    Canvas.setAttr(self.canvObj[1], ATTR_X2, 0)
    Canvas.setAttr(self.canvObj[1], ATTR_Y2, 0)
    Canvas.setAttr(self.canvObj[1], ATTR_X3, self.x+self.width)
    Canvas.setAttr(self.canvObj[1], ATTR_Y3, self.y+self.height)
    Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	if(create == true) then
		Canvas.add(self.parent.canvas, self.canvObj[1])
	end
end
function luaWidget.Timage:setOnTop()
	self:setObjOnTop()
end
function luaWidget.Timage:remove()
	Canvas.removeObj(self.parent.canvas, self.canvObj[1])
	self.parent = 0
end

--[[ Class Objet Texte ]]--
luaWidget.Ttext = luaWidget.object:new{text = "", align = _ALEFT, ctext = _CWHITE, cbackground = _CBLACK,
										shadow = false}
function luaWidget.Ttext:new(o)
	o = o or {}
	setmetatable(o,self)
	self.__index = self
	o.canvObj = {}
	o.canvObj[1] = Canvas.newFillRect(0,0,0,0,0)
	o.canvObj[2] = Canvas.newText(0,0,"")
	o.canvObj[3] = Canvas.newText(0,0,"")
	return o
end
function luaWidget.Ttext:modifObject(o)
	local i,j
	local chg = false
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
			if(i == "text") then chg = true end
		end
	end
	if(chg) then self.onValChange() end
	self.modif = true
end
function luaWidget.Ttext:prep(create)
	local text = string.sub(self.text,1,math.floor((self.width-2)/6))
	local xx = self.x +1
	local yy = self.y +1
	if(self.align == _ARIGHT) then
		xx = self.x + ((self.width-2)-(6* string.len(text)))
	elseif(self.align == _ACENTER) then
		xx = xx + math.floor(((self.width-2)-(6* string.len(text)))/2)
	elseif(self.align == _ATRONQ) then
		local nbchar = math.floor((self.width-2) /6)
		if(string.len(self.text) > nbchar) then
			local f = math.floor((40*nbchar)/100)
			local r = nbchar-f
			text = string.sub(self.text,1,f)..string.char(160)..string.sub(self.text,-r+1,-1)
		end
	end
	if(self.cbackground == _NONE) then
		Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, false)
	else
		Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x)
		Canvas.setAttr(self.canvObj[1], ATTR_Y1, self.y)
		Canvas.setAttr(self.canvObj[1], ATTR_X2, self.x+self.width)
		Canvas.setAttr(self.canvObj[1], ATTR_Y2, self.y+self.height)
		Canvas.setAttr(self.canvObj[1], ATTR_COLOR, self.cbackground)
		Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	end
	Canvas.setAttr(self.canvObj[2], ATTR_X1, xx+1)
	Canvas.setAttr(self.canvObj[2], ATTR_Y1, yy+1)
	Canvas.setAttr(self.canvObj[2], ATTR_TEXT, text)
	Canvas.setAttr(self.canvObj[2], ATTR_COLOR, _CDARKGRAY)
	if(self.visible) then
		Canvas.setAttr(self.canvObj[2], ATTR_VISIBLE, self.shadow)
	else
		Canvas.setAttr(self.canvObj[2], ATTR_VISIBLE, false)
	end
	Canvas.setAttr(self.canvObj[3], ATTR_X1, xx)
	Canvas.setAttr(self.canvObj[3], ATTR_Y1, yy)
	Canvas.setAttr(self.canvObj[3], ATTR_TEXT, text)
	Canvas.setAttr(self.canvObj[3], ATTR_COLOR, self.ctext)
	Canvas.setAttr(self.canvObj[3], ATTR_VISIBLE, self.visible)
	self.modif = false
	if(create == true) then
		Canvas.add(self.parent.canvas, self.canvObj[1])
		Canvas.add(self.parent.canvas, self.canvObj[2])
		Canvas.add(self.parent.canvas, self.canvObj[3])
	end
end
function luaWidget.Ttext:setOnTop()
	self:setObjOnTop()
end
function luaWidget.Ttext:remove()
	Canvas.removeObj(self.parent.canvas, self.canvObj[1])
	Canvas.removeObj(self.parent.canvas, self.canvObj[2])
	Canvas.removeObj(self.parent.canvas, self.canvObj[3])
	self.parent = 0
end

--[[ Class Objet Lien ]]--
luaWidget.Tlink = luaWidget.object:new{text = "",cvisited = _CPURPLE, visited = false, decoration = true,
	ctext = _CBLUE, cbackground = _CWHITE}
function luaWidget.Tlink:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	o.width = (string.len(o.text)*6)+3
	o.height = 11
	o.otext = luaWidget.Ttext:new{x=o.x, y=o.y, width=o.width, height = 11, text=o.text,
								 align=_ALEFT, ctext=o.ctext, cbackground=o.cbackground}
	o.canvObj = {}
	o.canvObj[1] = Canvas.newLine(0,0,0,0,0)
	return o
end
function luaWidget.Tlink:modifObject(o)
	local i,j
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
		end 
	end
	self.width = (string.len(self.text)*6)+3
	self.otext:modifObject{x=self.x, y=self.y, width=self.width, height = 11, text=self.text,
							align=_ALEFT, cbackground=self.cbackground}
	self.modif = true
end
function luaWidget.Tlink:prep(create)
	local yy = self.y +10
	if(self.decoration) then
		if(self.visited) then
			self.otext:modifObject{ctext=self.cvisited}
		else
			self.otext:modifObject{ctext=self.ctext}
		end
	else
		self.otext:modifObject{ctext=self.ctext}
	end
	self.otext:modifObject{visible=self.visible, parent=self.parent}
	self.otext:prep(create)
	Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x)
	Canvas.setAttr(self.canvObj[1], ATTR_Y1, yy)
	Canvas.setAttr(self.canvObj[1], ATTR_X2, self.x+self.width)
	Canvas.setAttr(self.canvObj[1], ATTR_Y2, yy)
	Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	if(self.decoration) then
		if(self.visited) then
			Canvas.setAttr(self.canvObj[1], ATTR_COLOR, self.cvisited)
			self.otext:modifObject{ctext=self.cvisited}
		else
			Canvas.setAttr(self.canvObj[1], ATTR_COLOR, self.ctext)
			self.otext:modifObject{ctext=self.ctext}
		end
	else
		Canvas.setAttr(self.canvObj[1], ATTR_COLOR, self.cbackground)
	end
	self.modif = false
	if(create == true) then
		Canvas.add(self.parent.canvas, self.canvObj[1])
	end
end
function luaWidget.Tlink:click()
	self.visited = true
	self.modif = true
	self:onClick()
end
function luaWidget.Tlink:setOnTop()
	self:setObjOnTop()
	self.otext:setOnTop()
end
function luaWidget.Tlink:remove()
	self.otext:remove()
	Canvas.removeObj(self.parent.canvas, self.canvObj[1])
	self.parent = 0
end

--[[ Class Objet EditBox ]]--
luaWidget.TeditBox = luaWidget.object:new{text = "", cborder = _CBLACK, cbackground = _CWHITE,
	ctext = _CBLACK, width = 100, height = 14}
function luaWidget.TeditBox:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	o.otext = luaWidget.Ttext:new{x=o.x+2, y=o.y+2, width=o.width-4, height =o.height-4, text=o.text,
								 align=o.align, ctext=o.ctext, cbackground=o.cbackground}
	o.canvObj = {}
	o.canvObj[1] = Canvas.newRect(0,0,0,0,0)
	o.canvObj[2] = Canvas.newFillRect(0,0,0,0,0)
	return o
end
function luaWidget.TeditBox:modifObject(o)
	local i,j
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
		end 
	end
	self.otext:modifObject{x=self.x+2, y=self.y+2, width=self.width-4, height=self.height-4, text=self.text,
							align=self.align, cbackground=self.cbackground, ctext=self.ctext}
	self.modif = true
	if(i == "text") then self:onValChange() end
end
function luaWidget.TeditBox:prep(create)
	Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x)
	Canvas.setAttr(self.canvObj[1], ATTR_Y1, self.y)
	Canvas.setAttr(self.canvObj[1], ATTR_X2, self.x+self.width)
	Canvas.setAttr(self.canvObj[1], ATTR_Y2, self.y+self.height)
	Canvas.setAttr(self.canvObj[1], ATTR_COLOR, self.cborder)
	Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[2], ATTR_X1, self.x+1)
	Canvas.setAttr(self.canvObj[2], ATTR_Y1, self.y+1)
	Canvas.setAttr(self.canvObj[2], ATTR_X2, self.x+self.width-1)
	Canvas.setAttr(self.canvObj[2], ATTR_Y2, self.y+self.height-1)
	Canvas.setAttr(self.canvObj[2], ATTR_COLOR, self.cbackground)
	Canvas.setAttr(self.canvObj[2], ATTR_VISIBLE, self.visible)
	self.modif = false
	if(create == true) then
		Canvas.add(self.parent.canvas, self.canvObj[1])
		Canvas.add(self.parent.canvas, self.canvObj[2])
		self.otext.parent=self.parent
	end
	self.otext:modifObject{visible=self.visible}
	self.otext:prep(create)
end
function luaWidget.TeditBox:click()
	self:onClick()
end
function luaWidget.TeditBox:dblClick()
	self:onDblClick()
end
function luaWidget.TeditBox:setOnTop()
	self:setObjOnTop()
	self.otext:setOnTop()
end
function luaWidget.TeditBox:remove()
	self.otext:remove()
	Canvas.removeObj(self.parent.canvas, self.canvObj[1])
	Canvas.removeObj(self.parent.canvas, self.canvObj[2])
	self.parent = 0
end

--[[ Class Objet Button ]]--
luaWidget.Tbutton = luaWidget.object:new{text = "", cbackground = _CGRAY, ctext = _CWHITE, width = 100,
	height = 16, check = false, bAssoc=_NONE, image="", visible=true}
function luaWidget.Tbutton:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	o.otext = luaWidget.Ttext:new{x=o.x+2, y=o.y+2, width=o.width-6, height=o.height-6, text=o.text,
					align=o.align, ctext=o.ctext, cbackground=o.cbackground, shadow=true, visible=o.visible}
	o.canvObj = {}
	o.canvObj[1] = Canvas.newFillRect(0,0,0,0,0)
	o.canvObj[2] = Canvas.newRect(0,0,0,0,_CWHITE)
	o.canvObj[3] = Canvas.newRect(0,0,0,0,_CBLACK)
	o.canvObj[4] = Canvas.newImage(0,0,0,0,0,0,0)
	if(o.image ~= "") then
		if(o.image ~= _NONE) then
			o.image = Image.load(o.image,VRAM)
		end
	end
	return o
end
function luaWidget.Tbutton:modifObject(o)
	local i,j
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
		end 
		if(i == "image") then
			if(self.image ~= "") then
				Image.destroy(self.image)
			end
			if(j == _NONE) then self.image = ""
			else
				self.image = Image.load(j,VRAM)
			end
		end
	end
	self.otext:modifObject{x=self.x+2, y=self.y+2, width=self.width-6, height=self.height-6, text=self.text,
					align=self.align, cbackground=self.cbackground, ctext=self.ctext, visible=self.visible}
	self.modif = true
end
function luaWidget.Tbutton:prep(create)
	local aa, bb, dep = 1,0,0
	local car = 0
	local lbut = {"Start","Select","A","B","X","Y","L","R","Up","Down","Left","Right"}
	local buff = self.text
	local ytc = 0
	if(self.bAssoc ~= nil) then
		if(self.bAssoc > 0) then
			buff = string.gsub(self.text,"%%B",lbut[self.bAssoc])
		end
	end
	if(self.check) then aa, bb = bb, aa end
	if(self.width > self.height) then car = self.height-6
	else car = self.width-6 end
	Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x)
	Canvas.setAttr(self.canvObj[1], ATTR_Y1, self.y)
	Canvas.setAttr(self.canvObj[1], ATTR_X2, self.x+self.width)
	Canvas.setAttr(self.canvObj[1], ATTR_Y2, self.y+self.height)
	Canvas.setAttr(self.canvObj[1], ATTR_COLOR, self.cbackground)
	Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[2], ATTR_X1, self.x+aa)
	Canvas.setAttr(self.canvObj[2], ATTR_Y1, self.y+aa)
	Canvas.setAttr(self.canvObj[2], ATTR_X2, self.x+self.width-bb)
	Canvas.setAttr(self.canvObj[2], ATTR_Y2, self.y+self.height-bb)
	Canvas.setAttr(self.canvObj[2], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[3], ATTR_X1, self.x)
	Canvas.setAttr(self.canvObj[3], ATTR_Y1, self.y)
	Canvas.setAttr(self.canvObj[3], ATTR_X2, self.x+self.width)
	Canvas.setAttr(self.canvObj[3], ATTR_Y2, self.y+self.height)
	Canvas.setAttr(self.canvObj[3], ATTR_VISIBLE, self.visible)
	ytc = math.floor(((self.height-4)/2)-3)
	Canvas.setAttr(self.canvObj[4], ATTR_X1, self.x+2+bb)
	Canvas.setAttr(self.canvObj[4], ATTR_Y1, self.y+ytc+bb)
	Canvas.setAttr(self.canvObj[4], ATTR_X3, car)
	Canvas.setAttr(self.canvObj[4], ATTR_Y3, car)
	Canvas.setAttr(self.canvObj[4], ATTR_IMAGE, self.image)
	Canvas.setAttr(self.canvObj[4], ATTR_VISIBLE, false)
	if(self.image ~= "") then
		Canvas.setAttr(self.canvObj[4], ATTR_VISIBLE, self.visible)
		dep = car+1
	end
	self.modif = false
	if(create == true) then
		Canvas.add(self.parent.canvas, self.canvObj[1])
		Canvas.add(self.parent.canvas, self.canvObj[2])
		Canvas.add(self.parent.canvas, self.canvObj[3])
		Canvas.add(self.parent.canvas, self.canvObj[4])
		self.otext.parent=self.parent
	end
	self.otext:modifObject{x=self.x+2+bb+dep, y=self.y+bb+ytc, text=buff, width=self.width-dep-6,
							visible=self.visible, ctext=self.ctext}
	if(not self.active)then self.otext:modifObject{ctext=self.cbackground} end
	self.otext:prep(create)
end
function luaWidget.Tbutton:click()
	self.check = true
	self.modif = true
end
function luaWidget.Tbutton:released(isOn)
	if(self.check) then
		if(isOn) then self:onClick() end
		self.check = false
		self.modif = true
	end
end
function luaWidget.Tbutton:heldKeys()
	local lbut = {"Start","Select","A","B","X","Y","L","R","Up","Down","Left","Right"}
	if(self.bAssoc ~= _NONE) then
		if(Keys.newPress[lbut[self.bAssoc]]) then
			self.check = true
			self:released(true)
		end
	end
end
function luaWidget.Tbutton:setOnTop()
	self:setObjOnTop()
	self.otext:setOnTop()
end
function luaWidget.Tbutton:remove()
	self.otext:remove()
	Canvas.removeObj(self.parent.canvas, self.canvObj[1])
	Canvas.removeObj(self.parent.canvas, self.canvObj[2])
	Canvas.removeObj(self.parent.canvas, self.canvObj[3])
	Canvas.removeObj(self.parent.canvas, self.canvObj[4])
	self.parent = 0
end

--[[ Class Objet CheckBox ]]--
luaWidget.TcheckBox = luaWidget.object:new{text = "", cbackground = _CBLACK, ctext = _CWHITE, width = 100,
	height = 10, check = false, align=_ALEFT}
function luaWidget.TcheckBox:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	if(o.align ~= _ALEFT) then
		o.align = _ARIGHT
	end
	o.otext = luaWidget.Ttext:new{x=o.x, y=o.y, width=o.width-10, height=o.height, text=o.text,
								align=o.align, ctext=o.ctext, cbackground=o.cbackground}
	o.canvObj = {}
	o.canvObj[1] = Canvas.newFillRect(0,0,0,0,_CWHITE)
	o.canvObj[2] = Canvas.newRect(0,0,0,0,_CBLACK)
	o.canvObj[3] = Canvas.newLine(0,0,0,0,_CBLACK)
	o.canvObj[4] = Canvas.newLine(0,0,0,0,_CBLACK)
	return o
end
function luaWidget.TcheckBox:modifObject(o)
	local i,j
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
		end 
	end
	if(self.align ~= _ALEFT) then
		self.align = _ARIGHT
	end
	self.otext:modifObject{x=self.x, y=self.y, width=self.width-10, height=self.height, text=self.text,
							align=self.align, cbackground=self.cbackground, ctext=self.ctext}
	self.modif = true
end
function luaWidget.TcheckBox:prep(create)
	local dec = 0
	if(self.align == _ALEFT) then
		self.otext:modifObject{x=self.x+10}
	else
		dec = self.width-10
		self.otext:modifObject{x=self.x}
	end
	Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x+dec)
	Canvas.setAttr(self.canvObj[1], ATTR_Y1, self.y)
	Canvas.setAttr(self.canvObj[1], ATTR_X2, self.x+10+dec)
	Canvas.setAttr(self.canvObj[1], ATTR_Y2, self.y+10)
	Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[2], ATTR_X1, self.x+dec)
	Canvas.setAttr(self.canvObj[2], ATTR_Y1, self.y)
	Canvas.setAttr(self.canvObj[2], ATTR_X2, self.x+10+dec)
	Canvas.setAttr(self.canvObj[2], ATTR_Y2, self.y+10)
	Canvas.setAttr(self.canvObj[2], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[3], ATTR_X1, self.x+2+dec)
	Canvas.setAttr(self.canvObj[3], ATTR_Y1, self.y+2)
	Canvas.setAttr(self.canvObj[3], ATTR_X2, self.x+8+dec)
	Canvas.setAttr(self.canvObj[3], ATTR_Y2, self.y+8)
	Canvas.setAttr(self.canvObj[3], ATTR_VISIBLE, self.check and self.visible)
	Canvas.setAttr(self.canvObj[4], ATTR_X1, self.x+8+dec)
	Canvas.setAttr(self.canvObj[4], ATTR_Y1, self.y+2)
	Canvas.setAttr(self.canvObj[4], ATTR_X2, self.x+2+dec)
	Canvas.setAttr(self.canvObj[4], ATTR_Y2, self.y+8)
	Canvas.setAttr(self.canvObj[4], ATTR_VISIBLE, self.check and self.visible)
	self.modif = false
	if(create == true) then
		Canvas.add(self.parent.canvas, self.canvObj[1])
		Canvas.add(self.parent.canvas, self.canvObj[2])
		Canvas.add(self.parent.canvas, self.canvObj[3])
		Canvas.add(self.parent.canvas, self.canvObj[4])
		self.otext.parent=self.parent
	end
	self.otext:modifObject{visible = self.visible}
	self.otext:prep(create)
end
function luaWidget.TcheckBox:click()
	self.check = not self.check
	self.modif = true
	self:onClick()
end
function luaWidget.TcheckBox:isCheck()
	return self.check
end
function luaWidget.TcheckBox:setCheck(check)
	if(check~=true) then check = false end
	self.check = check
	self.modif = true
	self:onValChange()
end
function luaWidget.TcheckBox:setOnTop()
	self:setObjOnTop()
	self.otext:setOnTop()
end
function luaWidget.TcheckBox:remove()
	self.otext:remove()
	Canvas.removeObj(self.parent.canvas, self.canvObj[1])
	Canvas.removeObj(self.parent.canvas, self.canvObj[2])
	Canvas.removeObj(self.parent.canvas, self.canvObj[3])
	Canvas.removeObj(self.parent.canvas, self.canvObj[4])
	self.parent = 0
end

--[[ Class Objet RadioButton Groupe]]--
luaWidget.TradioButton = luaWidget.object:new{lobj = {}, cbackground = _CBLACK, ctext = _CWHITE, width = 100,
	height = 10, select = 0, pos=_ALEFT}
function luaWidget.TradioButton:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	if(o.align ~= _ALEFT) then
		o.align = _ARIGHT
	end
	o.otext = {}
	o.canvObj = {}
	local i, cpt = 0, 0
	for i=1, #o.lobj do
		o.canvObj[cpt+1] = Canvas.newRect(0,0,0,0,_CBLACK)
		o.canvObj[cpt+2] = Canvas.newRect(0,0,0,0,_CBLACK)
		o.canvObj[cpt+3] = Canvas.newRect(0,0,0,0,_CBLACK)
		o.canvObj[cpt+4] = Canvas.newRect(0,0,0,0,_CBLACK)
		o.canvObj[cpt+5] = Canvas.newFillRect(0,0,0,0,_CWHITE)
		o.canvObj[cpt+6] = Canvas.newFillRect(0,0,0,0,_CWHITE)
		o.canvObj[cpt+7] = Canvas.newRect(0,0,0,0,_CWHITE)
		o.canvObj[cpt+8] = Canvas.newFillRect(0,0,0,0,_CBLACK)
		o.canvObj[cpt+9] = Canvas.newFillRect(0,0,0,0,_CBLACK)
		o.canvObj[cpt+10] = Canvas.newRect(0,0,0,0,_CBLACK)
		o.otext[i] = luaWidget.Ttext:new{x=o.x, y=o.y+((i-1)*10), width=o.width-10, height=10, 
			text=o.lobj[i], align=o.pos, ctext=o.ctext, cbackground=o.cbackground}
		cpt = cpt +10
	end
	o.height = #o.lobj*10
	return o
end
function luaWidget.TradioButton:modifObject(o)
	local i,j
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
		end 
	end
	if(self.pos ~= _ALEFT) then
		self.pos = _ARIGHT
	end
	local i
	for i=1, #self.lobj do
		self.otext[i] = luaWidget:modifObject{x=self.x, y=self.y+((i-1)*10), width=self.width-10, height=10,
							text=lobj[i], align=self.pos, ctext=self.ctext, cbackground=self.cbackground}
	end
	self.height = #self.lobj*10
	self.modif = true
end
function luaWidget.TradioButton:prep(create)
	local dec = 0
	local i, cpt = 0, 0
	local visible
	if(self.pos == _ALEFT) then
		for i=1, #self.lobj do
			self.otext[i]:modifObject{x=self.x+10}			
		end
	else
		dec = self.width-10
		for i=1, #self.lobj do
			self.otext[i]:modifObject{x=self.x}
		end
	end
	for i=1, #self.lobj do
		if(self.select == i) then visible = true
		else visible = false end
		Canvas.setAttr(self.canvObj[cpt+1], ATTR_X1, self.x+3+dec)
		Canvas.setAttr(self.canvObj[cpt+1], ATTR_Y1, self.y+cpt)
		Canvas.setAttr(self.canvObj[cpt+1], ATTR_X2, self.x+6+dec)
		Canvas.setAttr(self.canvObj[cpt+1], ATTR_Y2, self.y+9+cpt)
		Canvas.setAttr(self.canvObj[cpt+1], ATTR_VISIBLE, self.visible)
		Canvas.setAttr(self.canvObj[cpt+2], ATTR_X1, self.x+2+dec)
		Canvas.setAttr(self.canvObj[cpt+2], ATTR_Y1, self.y+1+cpt)
		Canvas.setAttr(self.canvObj[cpt+2], ATTR_X2, self.x+7+dec)
		Canvas.setAttr(self.canvObj[cpt+2], ATTR_Y2, self.y+8+cpt)
		Canvas.setAttr(self.canvObj[cpt+2], ATTR_VISIBLE, self.visible)
		Canvas.setAttr(self.canvObj[cpt+3], ATTR_X1, self.x+1+dec)
		Canvas.setAttr(self.canvObj[cpt+3], ATTR_Y1, self.y+2+cpt)
		Canvas.setAttr(self.canvObj[cpt+3], ATTR_X2, self.x+8+dec)
		Canvas.setAttr(self.canvObj[cpt+3], ATTR_Y2, self.y+7+cpt)
		Canvas.setAttr(self.canvObj[cpt+3], ATTR_VISIBLE, self.visible)
		Canvas.setAttr(self.canvObj[cpt+4], ATTR_X1, self.x+dec)
		Canvas.setAttr(self.canvObj[cpt+4], ATTR_Y1, self.y+3+cpt)
		Canvas.setAttr(self.canvObj[cpt+4], ATTR_X2, self.x+9+dec)
		Canvas.setAttr(self.canvObj[cpt+4], ATTR_Y2, self.y+6+cpt)
		Canvas.setAttr(self.canvObj[cpt+4], ATTR_VISIBLE, self.visible)
		Canvas.setAttr(self.canvObj[cpt+5], ATTR_X1, self.x+3+dec)
		Canvas.setAttr(self.canvObj[cpt+5], ATTR_Y1, self.y+1+cpt)
		Canvas.setAttr(self.canvObj[cpt+5], ATTR_X2, self.x+6+dec)
		Canvas.setAttr(self.canvObj[cpt+5], ATTR_Y2, self.y+8+cpt)
		Canvas.setAttr(self.canvObj[cpt+5], ATTR_VISIBLE, self.visible)
		Canvas.setAttr(self.canvObj[cpt+6], ATTR_X1, self.x+2+dec)
		Canvas.setAttr(self.canvObj[cpt+6], ATTR_Y1, self.y+2+cpt)
		Canvas.setAttr(self.canvObj[cpt+6], ATTR_X2, self.x+7+dec)
		Canvas.setAttr(self.canvObj[cpt+6], ATTR_Y2, self.y+7+cpt)
		Canvas.setAttr(self.canvObj[cpt+6], ATTR_VISIBLE, self.visible)
		Canvas.setAttr(self.canvObj[cpt+7], ATTR_X1, self.x+1+dec)
		Canvas.setAttr(self.canvObj[cpt+7], ATTR_Y1, self.y+3+cpt)
		Canvas.setAttr(self.canvObj[cpt+7], ATTR_X2, self.x+8+dec)
		Canvas.setAttr(self.canvObj[cpt+7], ATTR_Y2, self.y+6+cpt)
		Canvas.setAttr(self.canvObj[cpt+7], ATTR_VISIBLE, self.visible)
		Canvas.setAttr(self.canvObj[cpt+8], ATTR_X1, self.x+4+dec)
		Canvas.setAttr(self.canvObj[cpt+8], ATTR_Y1, self.y+2+cpt)
		Canvas.setAttr(self.canvObj[cpt+8], ATTR_X2, self.x+5+dec)
		Canvas.setAttr(self.canvObj[cpt+8], ATTR_Y2, self.y+7+cpt)
		Canvas.setAttr(self.canvObj[cpt+8], ATTR_VISIBLE, visible and self.visible)
		Canvas.setAttr(self.canvObj[cpt+9], ATTR_X1, self.x+3+dec)
		Canvas.setAttr(self.canvObj[cpt+9], ATTR_Y1, self.y+3+cpt)
		Canvas.setAttr(self.canvObj[cpt+9], ATTR_X2, self.x+6+dec)
		Canvas.setAttr(self.canvObj[cpt+9], ATTR_Y2, self.y+6+cpt)
		Canvas.setAttr(self.canvObj[cpt+9], ATTR_VISIBLE, visible and self.visible)
		Canvas.setAttr(self.canvObj[cpt+10], ATTR_X1, self.x+2+dec)
		Canvas.setAttr(self.canvObj[cpt+10], ATTR_Y1, self.y+4+cpt)
		Canvas.setAttr(self.canvObj[cpt+10], ATTR_X2, self.x+7+dec)
		Canvas.setAttr(self.canvObj[cpt+10], ATTR_Y2, self.y+5+cpt)
		Canvas.setAttr(self.canvObj[cpt+10], ATTR_VISIBLE, visible and self.visible)
		cpt = cpt +10
	end
	self.modif = false
	if(create == true) then
		for i=1, (#self.lobj*10) do
			Canvas.add(self.parent.canvas, self.canvObj[i])
		end
		for i=1, #self.lobj do
			self.otext[i].parent = self.parent
		end
	end
	for i=1, #self.lobj do
		self.otext[i]:modifObject{visible = self.visible}
		self.otext[i]:prep(create)
	end
end
function luaWidget.TradioButton:click()
	self.select = math.floor((Stylus.Y-self.y)/10)+1
	self.modif = true
	self:onClick()
end
function luaWidget.TradioButton:getSel()
	return self.select
end
function luaWidget.TradioButton:setSel(sel)
	if(sel<1) then sel = 1 end
	if(sel>#self.lobj) then sel = #self.lobj end
	self.select = sel
	self.modif = true
	self:onValChange()
end
function luaWidget.TradioButton:setOnTop()
	local i
	self:setObjOnTop()
	for i=1, #self.lobj do
		self.otext[i]:setOnTop()
	end
end
function luaWidget.TradioButton:remove()
	local i,j
	for i = 1, #self.lobj do
		self.otext[i]:remove()
		for j=1, 10 do
			Canvas.removeObj(self.parent.canvas, self.canvObj[j+(i-1)])
		end
	end
	self.parent = 0
end

--[[ Class Objet slider ]]--
luaWidget.Tslider = luaWidget.object:new{width = 100, height = 10, dir=_AHORIZONTAL, mode = _MSLIDER,
						cbackground = _CWHITE, pos = 1, min = 1, max = 100}
function luaWidget.Tslider:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	if(o.align ~= _ALEFT) then
		o.align = _ARIGHT
	end
	o.canvObj = {}
	o.canvObj[1] = Canvas.newRect(0,0,0,0,_CBLACK)
	o.canvObj[2] = Canvas.newFillRect(0,0,0,0,_CWHITE)
	o.canvObj[3] = Canvas.newRect(0,0,0,0,_CBLACK)
	o.canvObj[4] = Canvas.newFillRect(0,0,0,0,_CGRAY)
	return o
end
function luaWidget.Tslider:modifObject(o)
	local i,j
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			self[i] = j
			--if(i == "max" or i== "min") then self.pos = self.min end
		end 
	end
	if(self.dir ~= _AVERTICAL) then
		self.dir = _AHORIZONTAL
	end
	--if(self.max < self.min) then self.max, self.min = self.min, self.max end
	if(self.pos < self.min) then self.pos = self.min end
	if(self.pos > self.max) then self.pos = self.max end
	self.modif = true
end
function luaWidget.Tslider:prep(create)
	local xpos,ypos = self.x, self.y
	local larg, haut = self.width, self.height
	local vis = true
	local nbpos = self.max-(self.min-1)
	local nbvisi = 0
	local legalpos = self.max-self.min
	if(self.mode == _MLIFTER) then
		if(self.dir == _AVERTICAL) then
			nbvisi = math.floor((self.height-4)/12)
			legalpos = nbpos-(nbvisi-1)
			if(self.pos > legalpos) then self.pos = legalpos end
			haut = self.height/legalpos
			ypos = math.floor(haut*(self.pos-self.min))+self.y
			haut = math.floor(haut)
			if(legalpos < 1) then vis = false end
			if(haut < 4) then haut = 4 end
		else
			nbvisi = math.floor((self.width-4)/6)
			legalpos = nbpos-(nbvisi-1)
			if(self.pos > legalpos) then self.pos = legalpos end
			larg = self.width/legalpos
			xpos = math.floor(larg*(self.pos-self.min))+self.x
			larg = math.floor(larg)
			if(legalpos < 1) then vis = false end
			if(larg < 4) then larg = 4 end
		end
	elseif(self.mode == _MPROGRESS) then
		if(self.dir == _AVERTICAL) then
			--haut = math.floor((self.pos*self.height)/self.max)
			--larg = self.width-2
		else
			
		end
	else --mode = _MSLIDER
		if(self.dir == _AVERTICAL) then
			
		else
			
		end
	end
	Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x)
	Canvas.setAttr(self.canvObj[1], ATTR_Y1, self.y)
	Canvas.setAttr(self.canvObj[1], ATTR_X2, self.x+self.width)
	Canvas.setAttr(self.canvObj[1], ATTR_Y2, self.y+self.height)
	Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[2], ATTR_X1, self.x+1)
	Canvas.setAttr(self.canvObj[2], ATTR_Y1, self.y+1)
	Canvas.setAttr(self.canvObj[2], ATTR_X2, self.x+self.width-1)
	Canvas.setAttr(self.canvObj[2], ATTR_Y2, self.y+self.height-1)
	Canvas.setAttr(self.canvObj[2], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[3], ATTR_X1, xpos)
	Canvas.setAttr(self.canvObj[3], ATTR_Y1, ypos)
	Canvas.setAttr(self.canvObj[3], ATTR_X2, xpos+larg)
	Canvas.setAttr(self.canvObj[3], ATTR_Y2, ypos+haut)
	Canvas.setAttr(self.canvObj[3], ATTR_VISIBLE, vis and self.visible)
	Canvas.setAttr(self.canvObj[4], ATTR_X1, xpos+1)
	Canvas.setAttr(self.canvObj[4], ATTR_Y1, ypos+1)
	Canvas.setAttr(self.canvObj[4], ATTR_X2, xpos+larg-1)
	Canvas.setAttr(self.canvObj[4], ATTR_Y2, ypos+haut-1)
	Canvas.setAttr(self.canvObj[4], ATTR_VISIBLE, vis and self.visible)
	self.modif = false
	if(create == true) then
		Canvas.add(self.parent.canvas, self.canvObj[1])
		Canvas.add(self.parent.canvas, self.canvObj[2])
		Canvas.add(self.parent.canvas, self.canvObj[3])
		Canvas.add(self.parent.canvas, self.canvObj[4])
	end
end
function luaWidget.Tslider:click()
	self.cliq = true
end
function luaWidget.Tslider:held(isOn)
	if(self.cliq) then
		if(isOn) then
			if(self.dir == _AHORIZONTAL) then
				if(Stylus.deltaX<0) then self:setPos(self.pos-1)
				elseif(Stylus.deltaX>0) then self:setPos(self.pos+1)
				end
			else
				if(Stylus.deltaY<0) then self:setPos(self.pos-1)
				elseif(Stylus.deltaY>0) then self:setPos(self.pos+1)
				end
			end
			self:onValChange() 
		end
	end
end
function luaWidget.Tslider:released(isOn)
	self.cliq = nil
end
function luaWidget.Tslider:getPos()
	return self.pos
end
function luaWidget.Tslider:setPos(pos)
	if(pos<self.min) then pos = self.min end
	if(pos>self.max) then pos = self.max end
	self.pos = pos
	self.modif = true
	self:onValChange()
end
function luaWidget.Tslider:setOnTop()
	self:setObjOnTop()
end
function luaWidget.Tslider:remove()
	Canvas.removeObj(self.parent.canvas, self.canvObj[1])
	Canvas.removeObj(self.parent.canvas, self.canvObj[2])
	Canvas.removeObj(self.parent.canvas, self.canvObj[3])
	Canvas.removeObj(self.parent.canvas, self.canvObj[4])
	self.parent = nil
end

--[[ Class Objet ListView ]]--
luaWidget.TlistView = luaWidget.object:new{cbackground = _CWHITE, ctext = _CBLACK, width = 100,
	height = 30, multiSel = false, cselect = _CBLUE, start = 1, limage = {}}
function luaWidget.TlistView:new(o)
	local i
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	o.align = _ALEFT
	o.otext = {}
	o.canvObj = {}
	o.lobj = {}
	o.width = o.width -10
	o.tsel = {} --liste des num�ros des lignes s�lectionn�es
	o.timgObj = {} --table des indices d'image pour chaque objets
	o.maxvis = math.floor((o.height-4)/12) --maximum d'�l�ment visible
	o.olift = luaWidget.Tslider:new{mode = _MLIFTER, dir = _AVERTICAL, x=o.x+o.width, y=o.y,
									height = o.height, width = 10, pos=1, min=1, max=#o.lobj}
	o.canvObj[1] = Canvas.newRect(0,0,0,0,_CBLACK)
	o.canvObj[2] = Canvas.newFillRect(0,0,0,0,_CWHITE)
	local max = o.maxvis
	local xx = 2
	local txt,vis
	if(#o.limage>0) then xx = xx +12 end
	for i=1, max do
		txt,vis = "",false
		if(i<=#o.lobj) then
			txt = o.lobj[i]
			vis = true
		end
		o.otext[i] = luaWidget.Ttext:new{x=o.x+xx, y=o.y+2+((i-1)*12), width=o.width-12-xx, height=10,
					text=txt, align=_ALEFT, ctext=o.ctext, cbackground=o.cbackground, visible=vis}
		o.canvObj[i+2] = Canvas.newImage(0,0,0,0,0,10,10)
	end
	for i=1, #o.limage do
		o.limage[i] = Image.load(o.limage[i],VRAM)
	end
	function o.olift:onValChange()
		o.modif = true
		o.start = o.olift.pos
	end
	return o
end
function luaWidget.TlistView:modifObject(o)
	local i,j,k
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			if(i == "height") then j=self.height end
			if(i == "limage") then
				for k=1, #self.limage do
					Image.destroy(self.limage[k])
				end
			end
			self[i] = j
			if(i == "limage") then
				for k=1, #self.limage do
					self.limage[k] = Image.load(self.limage[k],VRAM)
				end
			elseif(i == "width") then
				self.width = j -10
				self.olift:modifObject{x=self.x + self.width}
			end
		end 
	end
	local max = #self.lobj
	if(#self.lobj>self.maxvis) then max = self.maxvis end
	local xx = 2
	if(#self.limage>0) then xx = xx +12 end
	for i=1, max do
		self.otext[i]:modifObject{x=self.x+xx, y=self.y+2+((i-1)*12), width=self.width-12-xx, 
							text=self.lobj[(self.start-1)+i], cbackground=self.cbackground, ctext=self.ctext}
	end
	self.olift:modifObject{x=self.x+self.width, y=self.y, height=self.height, width=10, max=#self.lobj, 
						   pos=self.start, min = 1}
	self.modif = true
end
function luaWidget.TlistView:prep(create)
	local dec = 0
	local i--, maxheight = 0, math.floor((self.height-4)/10)
	local img = _NONE
	local max = #self.lobj
	if(#self.lobj>self.maxvis) then max = self.maxvis end
	if((self.start+self.maxvis)>#self.lobj) then self.start = #self.lobj-self.maxvis+1 end
	if(self.start<1) then self.start = 1 end
	Canvas.setAttr(self.canvObj[1], ATTR_X1, self.x)
	Canvas.setAttr(self.canvObj[1], ATTR_Y1, self.y)
	Canvas.setAttr(self.canvObj[1], ATTR_X2, self.x+self.width)
	Canvas.setAttr(self.canvObj[1], ATTR_Y2, self.y+self.height)
	Canvas.setAttr(self.canvObj[1], ATTR_VISIBLE, self.visible)
	Canvas.setAttr(self.canvObj[2], ATTR_X1, self.x+1)
	Canvas.setAttr(self.canvObj[2], ATTR_Y1, self.y+1)
	Canvas.setAttr(self.canvObj[2], ATTR_X2, self.x+self.width-1)
	Canvas.setAttr(self.canvObj[2], ATTR_Y2, self.y+self.height-1)
	Canvas.setAttr(self.canvObj[2], ATTR_VISIBLE, self.visible)
	for i=1, self.maxvis do
		img = self.timgObj[(self.start-1)+i]
		Canvas.setAttr(self.canvObj[2+i], ATTR_X1, self.x+2)
		Canvas.setAttr(self.canvObj[2+i], ATTR_Y1, self.y+2+((i-1)*12))
		Canvas.setAttr(self.canvObj[2+i], ATTR_VISIBLE, false)
		Canvas.setAttr(self.canvObj[2+i], ATTR_IMAGE, 0)
		if(img ~= nil) then
			if(img > 0) then
				Canvas.setAttr(self.canvObj[2+i], ATTR_IMAGE, self.limage[img])
				Canvas.setAttr(self.canvObj[2+i], ATTR_VISIBLE, true and self.visible)
			end
		end
		self.otext[i]:modifObject{visible = true and self.visible}
	end
	self.modif = false
	if(create == true) then
		for i=1, #self.canvObj do
			Canvas.add(self.parent.canvas, self.canvObj[i])
		end
		self.parent:addObjects{self.olift}
		for i=1, self.maxvis do
			self.otext[i].parent=self.parent
		end
	end
	for i=1, self.maxvis do
		if(i <= max) then
			if(self.tsel[(self.start-1)+i]) then
				self.otext[i]:modifObject{text=self.lobj[(self.start-1)+i],cbackground=self.cselect}
			else
				self.otext[i]:modifObject{text=self.lobj[(self.start-1)+i],cbackground=self.cbackground}
			end
		else
			self.otext[i]:modifObject{visible= false}
		end
		self.otext[i]:prep(create)
	end
	self.olift:modifObject{visible=self.visible}
	self.olift:prep(create)
end
function luaWidget.TlistView:click()
	local nb = math.floor((Stylus.Y-self.y)/12)+self.start
	self:setSel(nb,true)
	self:onClick()
end
function luaWidget.TlistView:dblClick()
	self:onDblClick()
end
function luaWidget.TlistView:addElement(text, indim)
	if(type(text) == "table") then
		local i,j,txt,ind
		for i,j in pairs(text) do
			table.insert(self.lobj,j[1])
			table.insert(self.timgObj,j[2] or _NONE)
			table.insert(self.tsel, false)
		end
	else
		table.insert(self.lobj,text)
		table.insert(self.timgObj,indim or _NONE)
		table.insert(self.tsel, false)
	end
	self.olift:modifObject{max=#self.lobj}
	self.modif = true
end
function luaWidget.TlistView:delElement(indelem)
	if(indelem == "all") then
		self.lobj = {}
		self.timgObj = {}
		self.tsel = {}
	else
		table.remove(self.lobj,indelem)
		table.remove(self.timgObj,indelem)
		table.remove(self.tsel, indelem)
	end
	self.olift:modifObject{max=#self.lobj}
	self.modif = true
end
function luaWidget.TlistView:getSel()
	local i=0
	local tret = {}
	local tbuff = {}
	for i=1, #self.tsel do
		if(self.tsel[i]) then
			tbuff.num = i
			tbuff.text = self.lobj[i]
			tbuff.imind = self.timgObj[i] or _NONE
			table.insert(tret, tbuff)
		end
	end
	return tret
end
function luaWidget.TlistView:setSel(number,noMove)
	local i
	if(number < 1) then number = 1 end
	if(number > #self.lobj) then number = #self.lobj end
	if(not self.multiSel) then
		for i=1, #self.lobj do
			self.tsel[i] = false
		end
	end
	self.tsel[number] = not self.tsel[number]
	if(not noMove) then
		self.start = number - math.floor(self.maxvis/2)
		if(self.start < 1) then self.start = 1 end
		if(self.start+self.maxvis > #self.lobj+1) then self.start = (#self.lobj+1)-self.maxvis end
		self.olift.pos = self.start
	end
	self.modif = true
end
function luaWidget.TlistView:setOnTop()
	local i
	self:setObjOnTop()
	for i=1, self.maxvis do
		self.otext[i]:setOnTop()
	end
	self.olift:setOnTop()
end
function luaWidget.TlistView:remove()
	local i
	for i=1, #self.otext do
		self.otext[i]:remove()
	end
	for i=1, #self.canvObj do
		Canvas.removeObj(self.parent.canvas, self.canvObj[i])
	end
	self.parent:removeObjects{self.olift}
	self.parent = 0
end

--[[ Class Objet ComboBox ]]--
luaWidget.TcomboBox = luaWidget.object:new{cbackground = _CWHITE, ctext = _CBLACK, width = 100,
	height = 35, cselect = _CBLUE, selection = 1, text=""}
function luaWidget.TcomboBox:new(o)
	local i
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	o.deployed = false
	o.wchar = {19,20}
	o.oedit = luaWidget.TeditBox:new{x=o.x, y=o.y, width=o.width-13, ctext=o.ctext, height= 14,
										cbackground=o.cbackground, text = o.text}
	o.obutt = luaWidget.Tbutton:new{x=o.x+o.width-13, y=o.y, width=14, align=_ACENTER, height=14,
									active=o.active}
	o.olist = luaWidget.TlistView:new{x=o.x, y=o.y+13, width = o.width, height = o.height-13,
						ctext = o.ctext, cbackground = o.cbackground, cselect = o.cselect, active=false}
	if((o.olist.y+o.olist.height) > 192) then
		o.olist:modifObject{y=o.y-o.height+14}
		o.wchar = {20,19}
	end
	function o.obutt:onClick()
		o.deployed = not o.deployed
		o.olist:modifObject{active=deployed}
	end
	function o.olist:onDblClick()
		o.deployed = false
		o.text = o.olist:getSel()[1].text
		o.oedit:modifObject{text=o.text}
		o:onValChange()
	end
	function o.oedit:onDblClick()
		o:onDblClick()
	end
	function o.oedit:onClick()
		o:onClick()
	end
	return o
end
function luaWidget.TcomboBox:modifObject(o)
	local i,j,k
	for i,j in pairs(o) do
		if(self[i] ~= nil) then
			if(i == "height") then j=self.height end
			if(i == "text") then k=true end
			self[i] = j
		end 
	end
	self.wchar = {19,20}
	self.oedit:modifObject{x=self.x, y=self.y, width=self.width-13, ctext=self.ctext, height= 14,
							cbackground=self.cbackground, text=self.text}
	self.obutt:modifObject{x=self.x+self.width-13, y=self.y, width=14, align=_ACENTER, height=14,
							active=self.active}
	self.olist:modifObject{x=self.x, y=self.y+13, width = self.width, height = self.height-13,
						ctext = self.ctext, cbackground = self.cbackground, cselect = self.cselect}
	if((self.olist.y+self.olist.height) > 192) then
		self.olist:modifObject{y= self.y-self.height+14}
		self.wchar = {20,19}
	end
	self.modif = true
	if(k) then self:onValChange() end
end
function luaWidget.TcomboBox:prep(create)
	local i
	self.oedit:modifObject{text=self.text, visible=self.visible}
	if(self.deployed) then 
		self.obutt:modifObject{text=string.char(self.wchar[1])}
		self.olist:modifObject{visible=self.visible, active=self.visible}
	else 
		self.obutt:modifObject{text=string.char(self.wchar[2])}
		self.olist:modifObject{visible=false, active=false}
	end
	self.obutt:modifObject{visible=self.visible}
	if(create == true) then 
		self.parent:addObjects{self.oedit, self.obutt, self.olist}
	end
end
function luaWidget.TcomboBox:addElement(text, indim)
	self.olist:addElement(text, indim)
	self.modif=true
end
function luaWidget.TcomboBox:delElement(indelem)
	self.olist:delElement(indelem)
	self.modif=true
end
function luaWidget.TcomboBox:getSel()
	return self.text
end
function luaWidget.TcomboBox:setSel(number)
	self.olist:setSel(number)
	if(self.olist:getSel()[1] ~= nil) then
		self.text = self.olist:getSel()[1].text
	else
		self.text = ""
	end
	self.modif=true
	self:onValChange()
end
function luaWidget.TcomboBox:setOnTop()
	self.oedit:setOnTop()
	self.obutt:setOnTop()
	self.olist:setOnTop()
end
function luaWidget.TcomboBox:remove()
	self.parent:removeObjects{self.obutt, self.olist, self.oedit}
	self.parent = 0
end
