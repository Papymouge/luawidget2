require("standard")
dofile("luaWidget2.lua")

Debug.ON()
Debug.setColor(Color.new(31,12,15))
Debug.clear()
Controls.read()

fen1 = luaWidget.window:new{screen = SCREEN_DOWN}--, 
	--cbackground={Color.new(2,12,1),Color.new(2,12,1),Color.new(12,12,1),Color.new(12,12,1)}}

text1 = luaWidget.Ttext:new{x=10,y=10,text="Bonjour",ctext=Color.new(31,31,31),cbackground=Color.new(10,15,15), 
		align=_ACENTER}
lien1 = luaWidget.Tlink:new{x = 10, y = 25, text = "je me nomme"}
editB1 = luaWidget.TeditBox:new{x = 10, y = 40, text = "Blop", align=_ARIGHT}
button1 = luaWidget.Tbutton:new{x = 10, y = 55, text = "Bouton[%B]", bAssoc = _BL, image="Images/ok.png"}
check1 = luaWidget.TcheckBox:new{x = 10, y = 75, text = "Case � cocher"}--, align= _ARIGHT}
radio1 = luaWidget.TradioButton:new{x=10, y=100, lobj = {"Oui","Non"}}--, pos=_ARIGHT}
list1 = luaWidget.TlistView:new{x=100, y=130, limage={"Images/folder.png","Images/unknown.png"}}

combo = luaWidget.TcomboBox:new{x=10, y=130, width = 80, height=42}
combo:addElement({{"*.lua"},{"*.*"}})
combo:setSel(1)

text2 = luaWidget.Ttext:new{x=130,y=30,text="Bonjour",ctext=Color.new(31,31,31),cbackground=Color.new(10,15,15)}

--slid1 = luaWidget.Tslider:new{x=240, y=10, height = 150, width=10 , dir=_AVERTICAL, mode=_MLIFTER, max=1}
--slid2 = luaWidget.Tslider:new{x=10, y=180, height = 10, width=150 , dir=_AHORIZONTAL, mode=_MLIFTER, max=1}

list1:addElement("Texte1",2)
list1:addElement("Texte2",1)
list1:addElement("Texte3")
list1:addElement("Texte4")
list1:addElement({{"Texte5",1},{"Texte6",1},{"Texte7",2}})
list1:setSel(3)

text1.type="texte1"
lien1.type="lien1"
editB1.type="editBox1"
button1.type="button1"
check1.type="checkBox1"
radio1.type="radioButton1"
list1.type="liste1"
combo.type="comboBox"
text2.type="texte2"

function button1:onClick()
	editB1:modifObject{text="bip-bip"}
end


fen1:addObjects{text1, lien1, editB1, button1, check1, text2, radio1, list1, combo}

function fen1:onPopupClose(num)
	Debug.print("Bouton : "..num)
end

function myParent(me)
	local parent = false
	local i,j
	for i,j in pairs(_G) do
		if(j == me.parent) then parent=i break end
	end
	return parent
end

function myName(me)
	local name = false
	local i,j
	for i,j in pairs(_G) do
		if(j == me) then name=i break end
	end
	return name
end

val1 = true
val2 = true
val3 = true
while(not Keys.newPress.Start) do
	Controls.read()
	screen.print(SCREEN_DOWN,100,180,"Start pour quitter")
	if(Keys.newPress.A) then 
		--Debug.print(#fen1.lobj)
		fen1:modifObject{cbackground={Color.new(12,2,2),Color.new(12,2,2),Color.new(2,2,2),Color.new(2,2,2)}}
	end
	if(Keys.newPress.B) then
		combo:modifObject{y=170}
		--editB1.modif=true
		--combo.olist.active = true
	end
	if(Keys.newPress.X) then 
		val3 = not val3
		if(val3) then lien1:modifObject{text = "je"}
		else lien1:modifObject{text = "je me nomme"} end
	end
	if(Keys.newPress.Y) then 
		fen1:activePopup()--"Joli titre","J'ai quelquechose � dire!",{"OK!"})
	end
	if(Keys.newPress.Up) then 
		list1:modifObject{start=list1.start-1}
	end
	if(Keys.newPress.Down) then 
		list1:modifObject{start=list1.start+1}
	end
	--screen.drawFillRect(SCREEN_DOWN,0,0,256,192,Color.new(31,31,31))
	fen1:held()
	fen1:show()
	render()
end

fen1:destroy()

